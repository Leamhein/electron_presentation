const { app, BrowserWindow, ipcMain } = require('electron')

let valueWin;

function createBtnWindow() {
  // Создаём окно браузера.
  const btnWin = new BrowserWindow({
    width: 800,
    height: 600,
    //получить доступ к Node.js API и модулям. 
    // Node.js дает больше возможностей интеграции с ОС и позволяет воспользоваться дополнительными функциями системы.
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  btnWin.loadFile('button.html')

  // Отображаем средства разработчика.
  btnWin.webContents.openDevTools()

  // Будет вызвано, когда окно будет закрыто.
  btnWin.on('closed', () => {
    // Разбирает объект окна, обычно вы можете хранить окна     
    // в массиве, если ваше приложение поддерживает несколько окон в это время,
    // тогда вы должны удалить соответствующий элемент.
    btnWin = null
  })
}

function createValueWindow() {
  // Создаём окно браузера.
  valueWin = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })

  valueWin.loadFile('value.html');
  valueWin.webContents.openDevTools();
}

// Этот метод будет вызываться, когда Electron закончит 
// инициализацию и готов к созданию окон браузера.
// Некоторые API могут использоваться только после возникновения этого события.
app.on('ready', createBtnWindow);
app.on('ready', createValueWindow);

ipcMain.on('message', (event, data) => {
  valueWin.webContents.send('message', data);
})

global.sharedObject = {
  decreaseValue: 100,
}