const { ipcRenderer, remote } = require('electron')

function increase() {
  ipcRenderer.send('message', 'increase');
}

function decrease() {
  const currentValue = remote.getGlobal('sharedObject').decreaseValue;
  remote.getGlobal('sharedObject').decreaseValue = currentValue - 1;
}