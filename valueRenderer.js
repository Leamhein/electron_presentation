const { ipcRenderer, remote } = require('electron');

const increaseValueElement = document.getElementById('increaseValue');
const decreaseValueElement = document.getElementById('decreaseValue');

ipcRenderer.on('message', (event, data) => {
  console.log(event, data);

  const currentValue = +increaseValueElement.innerText;
  increaseValueElement.innerText = currentValue + 1;
});

function updateValue() {
  decreaseValueElement.innerText = remote.getGlobal('sharedObject').decreaseValue;
}

